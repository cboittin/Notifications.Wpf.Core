﻿using Notifications.Wpf.Core.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Notifications.Wpf.Core
{
    /// <summary>
    /// Implementation of the <see cref="INotificationManager"/> interface. Provides methods to show toast messages
    /// </summary>
    public class NotificationManager : INotificationManager
    {
        protected static readonly List<NotificationArea> Areas = new List<NotificationArea>();
        protected static NotificationsOverlayWindow? _window;

        protected readonly Dispatcher _dispatcher;
        protected readonly NotificationPosition _mainNotificationPosition;
        protected readonly DefaultNotificationPositionReference _defaultNotificationPositionRef;

        /// <summary>
        /// Creates an instance of the <see cref="NotificationManager"/>
        /// </summary>
        public NotificationManager() : this(DefaultNotificationPositionReference.Screen, NotificationPosition.BottomRight, null)
        {
        }

        /// <summary>
        /// Creates an instance of the <see cref="NotificationManager"/>
        /// </summary>
        /// <param name="positionRef">The reference target in which to position notifications with no custom area.</param>
        public NotificationManager(DefaultNotificationPositionReference positionRef) : this(positionRef, NotificationPosition.BottomRight, null)
        {
        }

        /// <summary>
        /// Creates an instance of the <see cref="NotificationManager"/>
        /// </summary>
        /// <param name="mainNotificationPosition">The position where notifications with no custom area should be displayed</param>
        public NotificationManager(NotificationPosition mainNotificationPosition) : this(DefaultNotificationPositionReference.Screen, mainNotificationPosition, null)
        {
        }

        /// <summary>
        /// Creates an instance of the <see cref="NotificationManager"/>
        /// </summary>
        /// <param name="positionRef">The reference target in which to position notifications with no custom area.</param>
        /// <param name="mainNotificationPosition">The position where notifications with no custom area should be displayed</param>
        public NotificationManager(DefaultNotificationPositionReference positionRef, NotificationPosition mainNotificationPosition) : this(positionRef, mainNotificationPosition, null)
        {
        }

        /// <summary>
        /// Creates an instance of the <see cref="NotificationManager"/>
        /// </summary>
        /// <param name="dispatcher">The <see cref="Dispatcher"/> that should be used</param>
        public NotificationManager(Dispatcher? dispatcher) : this(DefaultNotificationPositionReference.Screen, NotificationPosition.BottomRight, dispatcher)
        {
        }

        /// <summary>
        /// Creates an instance of the <see cref="NotificationManager"/>
        /// </summary>
        /// <param name="positionRef">The reference target in which to position notifications with no custom area.</param>
        /// <param name="mainNotificationPosition">The position where notifications with no custom area should be displayed</param>
        /// <param name="dispatcher">The <see cref="Dispatcher"/> that should be used</param>
        public NotificationManager(DefaultNotificationPositionReference positionRef, NotificationPosition mainNotificationPosition, Dispatcher? dispatcher)
        {
            _defaultNotificationPositionRef = positionRef;
            _mainNotificationPosition = mainNotificationPosition;

            if (dispatcher == null)
            {
                dispatcher = Application.Current?.Dispatcher ?? Dispatcher.CurrentDispatcher;
            }

            _dispatcher = dispatcher;
        }

        /// <inheritdoc cref="INotificationManager.ShowAsync(string, string?, TimeSpan?, Action?, Action?, CancellationToken)"/>
        public virtual async Task ShowAsync(string text, string? areaName = null, TimeSpan? expirationTime = null, Action? onClick = null,
            Action? onClose = null, CancellationToken token = default)
        {
            await ShowAsync(Guid.NewGuid(), text, areaName, expirationTime, (i) => onClick?.Invoke(), (i) => onClose?.Invoke(), token);
        }

        /// <inheritdoc/>
        public virtual async Task ShowAsync(Guid identifier, string text, string? areaName = null, TimeSpan? expirationTime = null,
            Action<Guid>? onClick = null, Action<Guid>? onClose = null, CancellationToken token = default)
        {
            await InternalShowAsync(identifier, text, areaName, expirationTime, onClick, onClose, token);
        }

        /// <inheritdoc/>
        public virtual async Task ShowAsync(NotificationContent content, string? areaName = null, TimeSpan? expirationTime = null,
            Action? onClick = null, Action? onClose = null, CancellationToken token = default)
        {
            await ShowAsync(Guid.NewGuid(), content, areaName, expirationTime, (i) => onClick?.Invoke(), (i) => onClose?.Invoke(), token);
        }

        /// <inheritdoc/>
        public virtual async Task ShowAsync(Guid identifier, NotificationContent content, string? areaName = null, TimeSpan? expirationTime = null,
            Action<Guid>? onClick = null, Action<Guid>? onClose = null, CancellationToken token = default)
        {
            await InternalShowAsync(identifier, content, areaName, expirationTime, onClick, onClose, token);
        }

        /// <inheritdoc/>
        public virtual async Task ShowAsync(UserControl notificationView, string? areaName = null, TimeSpan? expirationTime = null,
            Action? onClick = null, Action? onClose = null, CancellationToken token = default)
        {
            await ShowAsync(Guid.NewGuid(), notificationView, areaName, expirationTime, (i) => onClick?.Invoke(), (i) => onClose?.Invoke(), token);
        }

        /// <inheritdoc/>
        public virtual async Task ShowAsync(Guid identifier, UserControl notificationView, string? areaName = null, 
            TimeSpan? expirationTime = null, Action<Guid>? onClick = null, Action<Guid>? onClose = null, CancellationToken token = default)
        {
            await InternalShowAsync(identifier, notificationView, areaName, expirationTime, onClick, onClose, token);
        }

        public void ResetDefaultNotificationWindow()
        {
            var w = _window;
            _window = null;
            w?.Close();
        }

        internal static void AddArea(NotificationArea area)
        {
            Areas.Add(area);
        }

        internal static void RemoveArea(NotificationArea area)
        {
            Areas.Remove(area);
        }

        protected async Task InternalShowAsync(Guid identifier, object content, string? areaName, TimeSpan? expirationTime, Action<Guid>? onClick,
           Action<Guid>? onClose, CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                return;
            }

            if (!_dispatcher.CheckAccess())
            {
                await _dispatcher.BeginInvoke(
                    new Action(async () => await InternalShowAsync(identifier, content, areaName, expirationTime, onClick, onClose, token)));
                return;
            }

            if (expirationTime == null)
            {
                expirationTime = TimeSpan.FromSeconds(5);
            }

            if (string.IsNullOrEmpty(areaName))
            {
                areaName = "NotificationsOverlayWindow_NotifyArea";

                if (_window == null)
                {
                    if (this._defaultNotificationPositionRef == DefaultNotificationPositionReference.Screen)
                    {
                        var workArea = SystemParameters.WorkArea;

                        _window = new NotificationsOverlayWindow
                        {
                            Left = workArea.Left,
                            Top = workArea.Top,
                            Width = workArea.Width,
                            Height = workArea.Height,
                            Owner = Application.Current.MainWindow
                        };
                    }
                    else
                    {
                        _window = new NotificationsOverlayWindow
                        {
                            Left = Application.Current.MainWindow.Left,
                            Top = Application.Current.MainWindow.Top,
                            Width = Application.Current.MainWindow.ActualWidth,
                            Height = Application.Current.MainWindow.ActualHeight,
                            Owner = Application.Current.MainWindow
                        };
                    }

                    _window.SetNotificationAreaPosition(_mainNotificationPosition);
                    _window.Show();
                }
            }

            if (token.IsCancellationRequested)
            {
                return;
            }

            foreach (var area in Areas.Where(a => a.Identifier == areaName).ToList())
            {
                await area.ShowAsync(identifier, content, (TimeSpan)expirationTime, onClick, onClose, token);
            }
        }

        /// <inheritdoc cref="INotificationManager.CloseAsync(Guid)"/>
        public async Task CloseAsync(Guid identifier)
        {
            foreach (var area in Areas.ToList())
            {
                await area.CloseAsync(identifier);
            }
        }

        /// <inheritdoc cref="INotificationManager.CloseAllAsync"/>
        public async Task CloseAllAsync()
        {
            foreach (var area in Areas.ToList())
            {
                await area.CloseAllAsync();
            }
        }
    }
}