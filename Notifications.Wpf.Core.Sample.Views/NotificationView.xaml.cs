﻿namespace Notifications.Wpf.Core.Sample.Views
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for NotificationView.xaml.
    /// </summary>
    public partial class NotificationView : UserControl
    {
        private NotificationViewModel viewModel;

        public NotificationView(string title, string message, string okText = "OK")
        {
            this.InitializeComponent();
            this.viewModel = new NotificationViewModel()
            {
                Title = title,
                Message = message,
                OkText = okText,
            };
            this.DataContext = this.viewModel;
        }
    }
}