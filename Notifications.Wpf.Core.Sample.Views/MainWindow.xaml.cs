﻿using System;
using System.Timers;
using System.Windows;

namespace Notifications.Wpf.Core.Sample.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly NotificationManager _notificationManager = new NotificationManager(Controls.NotificationPosition.TopRight);
        private readonly Random _random = new Random();

        public MainWindow()
        {
            InitializeComponent();

            Timer timer = new Timer { Interval = 3000 };
            timer.Elapsed += (sender, args) =>
            {
                Application.Current.Dispatcher.Invoke(
                    async () => await _notificationManager.ShowAsync(new NotificationView("Notification", "This is a notification with a custom view."), "WindowArea")
                );
            };
            timer.Start();

            Closing += (s, e) =>
            {
                timer.Stop();
                timer.Dispose();
            };
        }
    }
}