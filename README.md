# Notifications.Wpf.Core

[![Version](https://img.shields.io/nuget/v/Notifications2.Wpf.Core.svg)](https://www.nuget.org/packages/Notifications2.Wpf.Core)  

Toast notifications for WPF apps based on .NET 6.0

This is a fork of [Notifications.Wpf.Core](https://github.com/mjuen/Notifications.Wpf.Core)

![Demo](https://i.imgur.com/UvYIVFV.gif)

### Installation:
```
Install-Package Notifications2.Wpf.Core
```
### Usage:

#### Notification over the taskbar:
```C#
var notificationManager = new NotificationManager();

await notificationManager.ShowAsync(new NotificationContent
{
    Title = "Sample notification",
    Message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    Type = NotificationType.Information
});
```

You can also alter this position by passing the desired position as an argument

```C#
var notificationManager = new NotificationManager(NotificationPosition.TopRight);
```

If you want the default position of notifications to be relative to your main window rather than the screen

```C#
var notificationManager = new NotificationManager(DefaultNotificationPositionReference.MainWindow);
```

#### Notification inside application window:
- Adding namespace:
```XAML
xmlns:notifications="clr-namespace:Notifications.Wpf.Core.Controls;assembly=Notifications.Wpf.Core"
```
- Adding new NotificationArea:
```XAML
<notifications:NotificationArea  
     MaxItems="3"
     x:Name="WindowArea"
     Position="TopLeft" />
```

It is also possible to add the area name with a `Binding`. But as binding to the `Name` property directly does not work, we offer you the `BindableName` property instead.

```XAML
<notifications:NotificationArea
    BindableName="{Binding NotificationAreaIdentifier}"
    MaxItems="3"
    Position="TopRight" />
```

- Displaying notification:
```C#
await notificationManager.ShowAsync(
    new NotificationContent {Title = "Notification", Message = "Notification in window!"},
    areaName: "WindowArea"
);
```


#### Simple text with OnClick & OnClose actions:
```C#
await notificationManager.ShowAsync("String notification",
    onClick: () => Console.WriteLine("Click"),
    onClose: () => Console.WriteLine("Closed!")
);
```

#### Notifications with identifiers

Sometimes it comes in handy if you can close specific notifications via code. To do that you have the possibility to specify an identifier in the form of a `Guid` for a notification.

```C#

var identifier = Guid.NewGuid(); 

await notificationManager.ShowAsync(identifier, "I'm here to stay", expirationTime: TimeSpan.MaxValue, 
    onClose: (id) => NotifySomeoneAboutClose(id));

await notificationManager.CloseAsync(identifier);
```


### MVVM support:
- NotificationViewModel:

```C#
using System.ComponentModel;

namespace Notifications.Wpf.Core.Sample.Views
{
    public class NotificationViewModel : INotifyPropertyChanged
    {
        private string title = string.Empty;
        private string message = string.Empty;
        private string okText = "OK";

        public event PropertyChangedEventHandler PropertyChanged;

        public string Title
        {
            get => this.title;
            set
            {
                if (this.title != value)
                {
                    this.title = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Title"));
                }
            }
        }

        public string Message
        {
            get => this.message;
            set
            {
                if (this.message != value)
                {
                    this.message = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Message"));
                }
            }
        }

        public string OkText
        {
            get => this.okText;
            set
            {
                if (this.okText != value)
                {
                    this.okText = value;
                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("OkText"));
                }
            }
        }
    }
}
```

- NotificationView:
```XAML
<UserControl x:Class="Notifications.Wpf.Core.Sample.Views.NotificationView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             xmlns:notif="clr-namespace:Notifications.Wpf.Core.Controls;assembly=Notifications.Wpf.Core"
             xmlns:local="clr-namespace:Notifications.Wpf.Core.Sample.Views" 
             mc:Ignorable="d" Foreground="Black" Background="Transparent">
    <Border Background="White" CornerRadius="7" Margin="10">
        <Border.Effect>
            <DropShadowEffect BlurRadius="10" ShadowDepth="0"/>
        </Border.Effect>
        <Grid Background="Transparent" VerticalAlignment="Stretch" Margin="20">
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="0.8*"/>
                <ColumnDefinition Width="0.2*"/>
            </Grid.ColumnDefinitions>
            <TextBlock Grid.Column="0" Text="{Binding Title}" FontWeight="Bold" TextTrimming="CharacterEllipsis" FontSize="22" HorizontalAlignment="Stretch" VerticalAlignment="Top"/>
            <TextBlock Grid.Column="0" Text="{Binding Message}" TextWrapping="Wrap" FontWeight="Regular" FontSize="17" HorizontalAlignment="Left" VerticalAlignment="Top" Margin="0,30,0,0"/>
            <Button Grid.Column="1" x:Name="Ok" Content="{Binding OkText}" DockPanel.Dock="Top" notif:Notification.CloseOnClick="True" MinWidth="60" Width="60" HorizontalAlignment="Right" VerticalAlignment="Center"/>
        </Grid>
    </Border>
</UserControl>
```
```C#
public partial class NotificationView : UserControl
{
    private NotificationViewModel viewModel;

    public NotificationView(string title, string message, string okText = "OK")
    {
        this.InitializeComponent();
        this.viewModel = new NotificationViewModel()
        {
            Title = title,
            Message = message,
            OkText = okText,
        };
        this.DataContext = this.viewModel;
    }
}
```

- Usage:
```C#
Application.Current.Dispatcher.Invoke(
    async () => await _notificationManager.ShowAsync(new NotificationView("Notification", "This is a notification with a custom view."), "WindowArea")
);
```
